import { Search } from "js-search";
import mentionData from "./mention-data";

const search = new Search("id");
search.addIndex(["attributes", "display_name"]);
search.addIndex(["attributes", "username"]);
search.addDocuments(mentionData.results);

export class MockMentionSource {
  constructor() {
    this.handlers = {};
  }
  
  query(query) {
    if (query && query.length >= 3) {
      const response = { query: query, results: search.search(query) } ;
      if (this.handlers["respond"]) {
        this.handlers["respond"](response);
      }
    }
  }
  
  on(eventName, handler) {
    this.handlers[eventName] = handler;
  }
}
