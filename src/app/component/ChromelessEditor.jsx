import React from "react";
import { Editor } from "@atlaskit/editor-core";
import { storyData as emojiStoryData } from "@atlaskit/emoji/dist/es5/support";
import { storyData as mentionStoryData } from "@atlaskit/mention/dist/es5/support";
import { MockActivityResource } from '@atlaskit/activity/dist/es5/support';


class ChromelessEditor extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      markdown: "markdown"
    };
    this.handleChange = this.handleChange.bind(this);
  }
  
  //TO-DO copy stuff from storybook
  
  onSubmit (editor) {
    alert(`Saved with python-markdown value: ${editor.value}`);
  }
  
  handleCancel(editor) {
  
  }
  
  handleChange (editor) {
    this.setState({
      markdown: editor.value
    });
  }
  
  render() {
    return (
      <div>
        <Editor
          appearance="chromeless"
          allowTextFormatting={true}
          allowTasksAndDecisions={true}
          allowHyperlinks={true}
          allowCodeBlocks={true}
          allowLists={true}
          allowTextColor={true}
          allowTables={true}
          allowJiraIssue={true}
          allowUnsupportedContent={true}
          allowInlineCommentMarker={true}
          allowPanel={true}
          allowInlineMacro={true}
          emojiProvider={emojiStoryData.getEmojiResource()}
          mentionProvider={Promise.resolve(mentionStoryData.resourceProvider)}
          activityProvider={Promise.resolve(new MockActivityResource())}
          saveOnEnter={true}
          //onSave={this.handleChange()}
  
          //placeholder="Write something..."
          shouldFocus={false}
        />
      </div>
    );
  }
}

export default ChromelessEditor;