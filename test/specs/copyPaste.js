import EditorPage from "./pages/editor.page";
import TestdataPage from "./pages/testdata.page";
import { expect } from "chai";

describe("copy-paste", function () {
  this.retries(2);
  it("should copy plain data", function () {
    TestdataPage.openTestdata();
    TestdataPage.copyPlainText();
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
    
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue(EditorPage.paste());
    expect(EditorPage.editable.getText().should.include("This text is plain."));
  });
  it("should copy data with markdown", function () {
    TestdataPage.openTestdata();
    TestdataPage.copyTestData();
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
    
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue(EditorPage.paste());
    expect(EditorPage.getText(EditorPage.strongText).should.be.include("bold "));
    expect(EditorPage.getText(EditorPage.strongText).should.be.include("italics and bold "));
    expect(EditorPage.getText(EditorPage.italicText).should.include("some italics only"));
    expect(EditorPage.getText(EditorPage.codeText).should.include("add some code to this"));
    expect(EditorPage.getText(EditorPage.underline).should.include("underline this text"));
    expect(EditorPage.getText(EditorPage.strikethrough).should.include("strikethough "));
    expect(EditorPage.getText(EditorPage.link).should.include("www.google.com"));
    expect(EditorPage.spanTextElement("blue is my fav color").should.not.be.null);
  });
  
  it("should copy data with table", function () {
    TestdataPage.openTestdata();
    TestdataPage.copyTestTable();
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
    
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue(EditorPage.paste());
    expect(EditorPage.getText(EditorPage.table).should.not.be.null);
  });
  
  it("should copy list", function () {
    TestdataPage.openTestdata();
    TestdataPage.copyList();
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
    
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue(EditorPage.paste());
    expect(EditorPage.getText(EditorPage.list).should.not.be.null);
  });
  
  it("should copy ordered list", function () {
    TestdataPage.openTestdata();
    TestdataPage.copyOrderedList();
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
  
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue(EditorPage.paste());
    expect(EditorPage.getText(EditorPage.orderedList).should.not.be.null);
    expect(EditorPage.getText(EditorPage.link).should.include("www.google.com"));
    expect(EditorPage.getText(EditorPage.strongText).should.include("format"));
    expect(EditorPage.getText(EditorPage.italicText).should.include("formatting"));
  });
});