import EditorPage from "./pages/editor.page";

describe("format", function () {
  const enter = "Enter";
  const space = "Space";
  
  before(() => {
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
  });
  
  this.retries(2);
  it("user should be able to create code block", function () {
    const moreCode = "```";
    EditorPage.editable.setValue([" this is "]);
    EditorPage.editable.addValue(moreCode);
    EditorPage.editable.addValue(enter);
    expect(browser.getElementSize("pre").should.not.be.null);
  });
  
  it("user should be able to enter bold",function(){
    const bold = "**bold**";
    EditorPage.editable.setValue("text");
    EditorPage.addText(bold);
    EditorPage.editable.addValue(enter);
    EditorPage.waitForElement(EditorPage.strongText);
    expect(EditorPage.getText(EditorPage.strongText).should.be.equal("bold"));
  });
  
  it("user should be able to enter italics",function(){
    const italics = "*italics* ";
    EditorPage.addText(italics);
    EditorPage.editable.addValue(space);
    EditorPage.waitForElement(EditorPage.italicText);
    expect(EditorPage.getText(EditorPage.italicText).should.be.equal("italics"));
  });
  
  it("user should be able to enter italics",function(){
    const code = "`code`";
    EditorPage.addText(code);
    EditorPage.editable.addValue(enter);
    EditorPage.waitForElement(EditorPage.codeText);
    expect(EditorPage.getText(EditorPage.codeText).should.contain("code"));
  });
  
  it("user should be able to enter heading1",function(){
    const heading = "# Heading";
    EditorPage.editable.addValue([heading]);
    EditorPage.editable.addValue(enter);
    EditorPage.waitForElement(EditorPage.headingText("1"));
    expect(EditorPage.getText(EditorPage.headingText("1")).should.contain("Heading"));
  });
  
  it("user should be able to enter heading2",function(){
    const heading = "## Heading";
    EditorPage.editable.addValue([heading]);
    EditorPage.editable.addValue(enter);
    EditorPage.waitForElement(EditorPage.headingText("2"));
    expect(EditorPage.getText(EditorPage.headingText("2")).should.contain("Heading"));
  });
  
  it("user should be able to enter heading3",function(){
    const heading = "### Heading";
    EditorPage.editable.addValue([heading]);
    EditorPage.editable.addValue(enter);
    EditorPage.waitForElement(EditorPage.headingText("3"));
    expect(EditorPage.getText(EditorPage.headingText("3")).should.contain("Heading"));
  });
  
  it("user should be able to enter heading4",function(){
    const heading = "#### Heading";
    EditorPage.editable.addValue([heading]);
    EditorPage.editable.addValue(enter);
    EditorPage.waitForElement(EditorPage.headingText("4"));
    expect(EditorPage.getText(EditorPage.headingText("4")).should.contain("Heading"));
  });
  
  it("user should be able to enter heading5",function(){
    const heading = "##### Heading";
    EditorPage.editable.addValue([heading]);
    EditorPage.editable.addValue(enter);
    EditorPage.waitForElement(EditorPage.headingText("5"));
    expect(EditorPage.getText(EditorPage.headingText("5")).should.contain("Heading"));
  });
  
  it("user should be able to create list items",function(){
    const slNo  = "1. ";
    const listContent = "text";
    const listElement = "ol";
    // EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue([slNo,"Space",listContent]);
    EditorPage.waitForElement(listElement);
    expect(EditorPage.getText(listElement).should.contain(listContent));
  });
  
  it("create link element using markdown", function() {
    const markdown = "[link](https://hello.com)";
    const input = "link";
    EditorPage.editable.setValue("text");
    EditorPage.addText(markdown);
    expect(EditorPage.getText("a").toLowerCase().should.be.equal(input));
  });
  
});