import EditorPage from "./pages/editor.page";
import { expect }from "chai";

describe("list", function () {
  before(() => {
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
  });

  this.retries(2);
  //Open or close insert block dropdown
  it("create nested unordered list", function () {
    EditorPage.open();
    const unorderedListButton = EditorPage.getTypeElement("Unordered list");
    const input1 = "item 1";
    const input2 = "item 2";
    const enter = "Enter";
    const tab = "Tab";
    const output = "item 1\nitem 1\nitem 2\nitem 2";
    const markdown = "* item 1\n\n    * item 1\n    * item 2\n    \n* item 2";
    const unorderedList = "ul";

    browser.waitForExist(unorderedListButton);
    EditorPage.click(unorderedListButton);
    browser.waitForEnabled(unorderedList);
    EditorPage.editable.addValue(input1);
    EditorPage.editable.addValue(enter);
    EditorPage.editable.addValue(tab);
    EditorPage.editable.addValue(input1);
    EditorPage.editable.addValue(enter);
    EditorPage.editable.addValue(input2);
    EditorPage.editable.addValue(enter);
    EditorPage.editable.addValue(enter);
    EditorPage.editable.addValue(input2);
    browser.waitForExist(unorderedList);

    expect(EditorPage.getText(unorderedList)[0].should.contain(output));
    if(EditorPage.isBitbucket())
      expect(EditorPage.markdown.should.contain(markdown));
  });

  //Ordered list
  it("create nested ordered list", function () {
    EditorPage.open();
    const orderedListButton = EditorPage.getTypeElement("Ordered list");
    browser.waitForExist(orderedListButton);
    EditorPage.click(orderedListButton);

    const input1 = "item 1";
    const input2 = "item 2";
    const enter = "Enter";
    const tab = "Tab";
    const output = "item 1\nitem 1\nitem 2\nitem 2";
    const markdown = "1. item 1\n\n    1. item 1\n    2. item 2\n    \n2. item 2";
    const orderedList = "ol";
    browser.waitForEnabled(orderedList);
    EditorPage.editable.addValue(input1);
    EditorPage.editable.addValue(enter);
    EditorPage.editable.addValue(tab);
    EditorPage.editable.addValue(input1);
    EditorPage.editable.addValue(enter);
    EditorPage.editable.addValue(input2);
    EditorPage.editable.addValue(enter);
    EditorPage.editable.addValue(enter);
    EditorPage.editable.addValue(input2);
    browser.waitForExist(orderedList);

    expect(EditorPage.getText(orderedList)[0].should.contain(output));
    if(EditorPage.isBitbucket())
      expect(EditorPage.markdown.should.contain(markdown));
  });
});
